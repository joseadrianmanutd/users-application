﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UsersApplication
{
    public class GenderValidationAttrubute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            if (value != null && (value.ToString() == "M" || value.ToString() == "F"))
            {
                return ValidationResult.Success;
            }


            return new ValidationResult("The gender should be M or F");
        }
    }
}