namespace UsersApplication.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using UsersApplication.Models;


    public class ApplicationDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            userManager.Create(
                new ApplicationUser
                {
                    UserName = "username1",
                    Email = "test_user@email.com",
                    IsActive = true,
                    Gender = "M"
                }, "!Password1");

            userManager.Create(
                new ApplicationUser
                {
                    UserName = "username2",
                    Email = "test_user2@email.com",
                    IsActive = false,
                    Gender = "M"
                }, "!Password1");

            userManager.Create(
                new ApplicationUser
                {
                    UserName = "username3",
                    Email = "test_user3@email.com",
                    IsActive = false,
                    Gender = "M"
                }, "!Password1");

            context.SaveChanges();
        }
    }
}
