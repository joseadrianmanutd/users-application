# README #

### How do I get set up? ###

* Change connection string in the API project (database name), the database will be created automatically
* Run "bower install" in the web project path
* In set startup projects select multiple project (Both must be running)

* Make sure that apiUrl constant in UsersApplication/UsersApplication/App/Config/constants.js contains the same port the API is running (should work by default)
