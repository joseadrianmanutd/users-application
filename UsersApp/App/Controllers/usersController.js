﻿(function () {
    'use strict';
    angular.module('usersApp')
        .controller('usersController',
        ['accountService', '$mdDialog', '$scope', '$state',
            function (accountService, $mdDialog, $scope, $state) {
                var $ctrl = this;

                $ctrl.logout = function () {
                    accountService.logout();
                    $state.go("login", {});
                };

                $ctrl.getUsers = function () {
                    accountService.getUsers().then(function (response) {
                        $ctrl.users = response.data;
                        $ctrl.userName = accountService.getUserName();
                    }, function (response) {
                        //Show error
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('Error')
                                .textContent('Please log in again.')
                                .ok('DISMISS')
                        );
                        $state.go("login");
                    })
                };

                $ctrl.changeStatus = function (id, isActive) {
                    var confirm = $mdDialog.confirm()
                        .title('Would you like to change the status of this use to ' + (isActive ? 'inactive' : 'active') + '?')
                        .textContent('')
                        .ariaLabel('')
                        .ok('Yes')
                        .cancel('No');

                    $mdDialog.show(confirm).then(function () {
                        accountService.changeStatus(id, isActive)
                            .then(function (response) {
                                $ctrl.getUsers();
                            }, function () {
                            });
                    }, function () {
                    });
                };

                $ctrl.showChangePasswordModal = function(id) {
                    $mdDialog.show({
                        templateUrl: '/App/Views/Modals/changePasswordModal.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: true,
                        fullscreen: true,
                        controllerAs: '$changePasswordModal',
                        controller: function () {
                            var $changePasswordCtrl = this;
                            
                            $changePasswordCtrl.change = function () {
                                var model = {
                                    OldPassword: $changePasswordCtrl.currentPassword,
                                    NewPassword: $changePasswordCtrl.newPassword,
                                    ConfirmPassword: $changePasswordCtrl.confirmNewPassword
                                };
                                accountService.changePassword(model)
                                    .then(function (response) {
                                        accountService.logout();
                                        $mdDialog.hide(true);
                                        //Show message
                                        $mdDialog.show(
                                            $mdDialog.alert()
                                                .parent(angular.element(document.querySelector('#popupContainer')))
                                                .clickOutsideToClose(true)
                                                .title('Password changed')
                                                .textContent('The password has changed succesfully.')
                                                .ok('Ok')
                                        ).finally(function () {
                                            $state.go("login");
                                        });
                                    }, function (response) {
                                        $changePasswordCtrl.error_message = response.data.ModelState;
                                    });
                            };

                            $changePasswordCtrl.close = function () {
                                $mdDialog.hide(true);
                            };
                        }
                    }).then(function (success) {
                    }).then(function (cancelData) {
                    });
                }

                    $ctrl.showModal = function (user) {
                        $mdDialog.show({
                            templateUrl: '/App/Views/Modals/usersEditorModal.html',
                            parent: angular.element(document.body),
                            clickOutsideToClose: true,
                            fullscreen: true,
                            controllerAs: '$editorModal',
                            controller: function () {
                                var $editorCtrlModal = this;
                                $editorCtrlModal.user = {};

                                if (user !== undefined) {
                                    $editorCtrlModal.user = user;
                                    $editorCtrlModal.user.Status = user.IsActive ? 'active' : 'inactive';
                                    $editorCtrlModal.activeUserName = $ctrl.userName;
                                }      
                                else {
                                    $editorCtrlModal.user.Status = 'active';                                    
                                }

                                $editorCtrlModal.save = function () {
                                    var model = {
                                        UserId: $editorCtrlModal.user.Id,
                                        Email: $editorCtrlModal.user.Email,
                                        User: $editorCtrlModal.user.UserName,
                                        IsActive: $editorCtrlModal.user.Status == 'active',
                                        Gender: $editorCtrlModal.user.Gender,
                                        Password: $editorCtrlModal.user.newPassword,
                                        ConfirmPassword: $editorCtrlModal.user.confirmNewPassword
                                    };
                                    if (model.UserId === undefined) {
                                        accountService.add(model)
                                            .then(function (response) {  
                                                $mdDialog.hide(true);
                                            }, function (response) {
                                                $editorCtrlModal.error_message = response.data.ModelState;
                                            });
                                    }
                                    else {
                                        accountService.update(model)
                                            .then(function (response) {                                       
                                                $mdDialog.hide(true);
                                            }, function (response) {
                                                $editorCtrlModal.error_message = response.data.ModelState;
                                            });
                                    }
                                };

                                $editorCtrlModal.close = function () {
                                    $mdDialog.hide(true);
                                };
                            }
                        }).then(function (success) {
                            $ctrl.getUsers();
                        }).then(function (cancelData) {

                        });
                    }

                $ctrl.getUsers();
            }
        ]);
})();