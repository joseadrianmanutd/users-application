﻿(function () {
    'use strict';
    angular.module('usersApp')
        .controller('accountController',
        ['accountService', '$mdDialog', '$scope', '$state',
            function (accountService, $mdDialog, $scope, $state) {
                var $ctrl = this;                

                $ctrl.redirectToUsersView = function(user, password) {
                    accountService.getToken(user, password).then(function (response) {
                        $state.go("users", {});
                    }, function (response) {
                        $ctrl.error_message = response.data.error_description;
                    });                 
                };
                $ctrl.redirectToUsersView();
            }
        ]);
})();