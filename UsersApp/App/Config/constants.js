﻿(function () {
    'use strict';
    angular.module('usersApp')     
        .constant('usersApi',
        {
            'apiUrl': 'http://localhost:53508/api/'
        })
})();
