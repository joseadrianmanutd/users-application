﻿(function () {
    'use strict';
    angular.module("usersApp").config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('login', {
                url: "/",
                templateUrl: "App/Views/account.html",
                controller: 'accountController',
                controllerAs: '$ctrl'
            })
            .state("users", {
                url: "/users",
                templateUrl: "App/Views/users.html",
                controller: 'usersController',
                controllerAs: '$ctrl',
            });
        $urlRouterProvider.otherwise('/');
    }]);
})();