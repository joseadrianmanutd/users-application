﻿(function () {
    'use strict';
    angular.module('usersApp')
        .service('accountService', [
            '$http', 'usersApi', '$cookies', '$q',
            function ($http, usersApi, $cookies, $q) {
                this.action = 'account/';
                //this.add = function (image) {
                //    var request = $http({
                //        method: 'POST',
                //        url: usersApi.apiUrl + this.action,
                //        data: JSON.stringify(image),
                //        dataType: "json"
                //    });
                //    return request;
                //}

                //this.update = function (image) {
                //    var request = $http({
                //        method: 'PUT',
                //        url: usersApi.apiUrl + this.action + image.Id,
                //        data: JSON.stringify(image),
                //        dataType: "json"
                //    });
                //    return request;
                //}

                //this.delete = function (id) {
                //    var request = $http({
                //        method: 'DELETE',
                //        url: usersApi.apiUrl + this.action + id,
                //        dataType: "json"
                //    });
                //    return request;
                //}

                //this.getAll = function () {
                //    var request = $http({
                //        method: 'GET',
                //        url: usersApi.apiUrl + this.action,
                //        dataType: "json"
                //    });
                //    return request;
                //}

                this.getToken = function (user, password) {
                    var obj = $q.defer();
                    var token = $cookies.get('token');
                    var tokenExpiryDate = $cookies.get('tokenExpiryDate');
                    if (token != null) {
                        if (new Date(tokenExpiryDate) > new Date()) {
                            obj.resolve(token);
                        }
                        else {
                            this.generateToken(user, password).then(function (response) {
                                $cookies.put('token', response.data.access_token);
                                $cookies.put('tokenExpiryDate', response.data['.expires']);
                                $cookies.put('userName', user);
                                obj.resolve(response.data.access_token);
                            }, function (response) {
                                obj.reject(response);
                            });
                        }
                    }
                    else if (user != null && password != null) {
                        this.generateToken(user, password).then(function (response) {
                            $cookies.put('token', response.data.access_token);
                            $cookies.put('tokenExpiryDate', response.data['.expires']);
                            $cookies.put('userName', user);
                            obj.resolve(response.data.access_token);
                        }, function (response) {
                            obj.reject(response);
                        });
                    }
                    return obj.promise;
                }

                this.getTokenFromCookies = function () {                    
                    var token = $cookies.get('token');
                    var tokenExpiryDate = $cookies.get('tokenExpiryDate');
                    if (token != null) {
                        if (new Date(tokenExpiryDate) > new Date()) {
                            return token;
                        }                       
                    }                   
                    return false;
                }

                this.getUserName = function () {
                    return $cookies.get('userName');  
                }
                
                this.generateToken = function (user, password) {
                    var data = "grant_type=password&username=" + user + "&password=" + password;
                    var request = $http({
                        method: 'POST',
                        url: usersApi.apiUrl + 'token',
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                        data:  data,
                        dataType: "json"
                    });
                    return request;
                }

                this.logout = function () {
                    $cookies.remove('token');
                    $cookies.remove('tokenExpiryDate');
                    $cookies.remove('userName');
                }

                this.getUsers = function () {
                    var token = this.getTokenFromCookies();
                    var request = $http({
                        method: 'GET',
                        url: usersApi.apiUrl + 'Account/users',
                        headers: { 'Authorization': 'Bearer ' + token },
                        dataType: "json"
                    });
                    return request;
                }

                this.changeStatus = function (id, isActive) {
                    var token = this.getTokenFromCookies();
                    var request = $http({
                        method: 'POST',
                        url: usersApi.apiUrl + 'Account/ChangeState',
                        headers: { 'Authorization': 'Bearer ' + token },
                        dataType: "json",
                        data: JSON.stringify({ UserId: id, IsActive: !isActive })
                    });
                    return request;
                }

                this.changePassword = function (model) {
                    var token = this.getTokenFromCookies();
                    var request = $http({
                        method: 'POST',
                        url: usersApi.apiUrl + 'Account/ChangePassword',
                        headers: { 'Authorization': 'Bearer ' + token },
                        dataType: "json",
                        data: JSON.stringify(model)
                    });
                    return request;
                }

                this.update = function (model) {
                    var token = this.getTokenFromCookies();
                    var request = $http({
                        method: 'POST',
                        url: usersApi.apiUrl + 'Account/UpdateUser',
                        headers: { 'Authorization': 'Bearer ' + token },
                        dataType: "json",
                        data: JSON.stringify(model)
                    });
                    return request;
                }

                this.add = function (model) {
                    var request = $http({
                        method: 'POST',
                        url: usersApi.apiUrl + 'Account/Register',
                        dataType: "json",
                        data: JSON.stringify(model)
                    });
                    return request;
                }
            }
        ]);
})()

    